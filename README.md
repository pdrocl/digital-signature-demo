Demo class that allows to digital sign messages (also known as CMS or PKCS7) using the Bouncy Castle provider. 
Please note this is a demo specifically created to be used with jre 1.4.

Look at the junit tests to see examples of how the class must be used. There's a self signed certificate included but any p12 file should work