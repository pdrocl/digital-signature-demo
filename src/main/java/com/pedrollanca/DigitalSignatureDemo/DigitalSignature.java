package com.pedrollanca.DigitalSignatureDemo;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Security;
import java.security.Signature;
import java.security.SignatureException;
import java.security.UnrecoverableKeyException;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.jcajce.JcaCertStore;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import org.bouncycastle.cms.CMSException;
import org.bouncycastle.cms.CMSProcessable;
import org.bouncycastle.cms.CMSProcessableByteArray;
import org.bouncycastle.cms.CMSSignedData;
import org.bouncycastle.cms.CMSSignedDataGenerator;
import org.bouncycastle.cms.CMSTypedData;
import org.bouncycastle.cms.SignerInformation;
import org.bouncycastle.cms.SignerInformationStore;
import org.bouncycastle.cms.jcajce.JcaSignerInfoGeneratorBuilder;
import org.bouncycastle.cms.jcajce.JcaSimpleSignerInfoVerifierBuilder;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.operator.ContentSigner;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;
import org.bouncycastle.operator.jcajce.JcaDigestCalculatorProviderBuilder;
import org.bouncycastle.util.Store;
import org.bouncycastle.util.encoders.Base64;

import sun.security.pkcs.ContentInfo;
import sun.security.pkcs.PKCS7;
import sun.security.pkcs.SignerInfo;
import sun.security.util.DerOutputStream;
import sun.security.util.DerValue;
import sun.security.x509.AlgorithmId;
import sun.security.x509.X500Name;
import java.security.cert.X509Certificate;
import java.security.Signature;
import java.math.BigInteger;

/**
 * Demo class that allows to digital sign messages (also known as CMS or PKCS7) using the Bouncy Castle provider. 
 * Please note this is a demo specifically created to be used with jre 1.4.
 * 
 * @author Pedro Chamorro
 * 
 *
 */
class DigitalSignature {

	private static String PATH_TO_KEYSTORE;
	private static String KEY_ALIAS_IN_KEYSTORE;
	private static String KEYSTORE_PASSWORD;
	private static String SIGNATURE_ALGORITHM;
	private KeyStore keystore;
	private CMSSignedDataGenerator generator;
	private Store certstore;
	private Certificate cert;
	private ContentSigner signer;

	/**
	 * @param path
	 * @param alias
	 * @param password
	 * @throws KeyStoreException
	 * @throws NoSuchAlgorithmException
	 * @throws CertificateException
	 * @throws IOException
	 * @throws UnrecoverableKeyException
	 * @throws OperatorCreationException
	 * @throws CMSException
	 */
	public DigitalSignature(String path, String alias, String password)
			throws KeyStoreException, NoSuchAlgorithmException, CertificateException, IOException,
			UnrecoverableKeyException, OperatorCreationException, CMSException {
		PATH_TO_KEYSTORE = path;
		KEY_ALIAS_IN_KEYSTORE = alias;
		KEYSTORE_PASSWORD = password;
		SIGNATURE_ALGORITHM = "SHA1withRSA";
		Security.addProvider(new BouncyCastleProvider());
		generator = new CMSSignedDataGenerator();
		loadKeyStore();
		setUpProvider();
	}

	/**
	 * @throws KeyStoreException
	 * @throws NoSuchAlgorithmException
	 * @throws CertificateException
	 * @throws IOException
	 */
	private void loadKeyStore() throws KeyStoreException, NoSuchAlgorithmException, CertificateException, IOException {
		keystore = KeyStore.getInstance("PKCS12");
		InputStream is = new FileInputStream(PATH_TO_KEYSTORE);
		keystore.load(is, KEYSTORE_PASSWORD.toCharArray());
		is.close();
	}

	/**
	 * @throws KeyStoreException
	 * @throws CertificateEncodingException
	 * @throws UnrecoverableKeyException
	 * @throws OperatorCreationException
	 * @throws NoSuchAlgorithmException
	 * @throws CMSException
	 */
	private void setUpProvider() throws KeyStoreException, CertificateEncodingException, UnrecoverableKeyException,
		OperatorCreationException, NoSuchAlgorithmException, CMSException {
		Certificate[] certchain = (Certificate[]) keystore.getCertificateChain(KEY_ALIAS_IN_KEYSTORE);
		final List certlist = new ArrayList();

		for (int i = 0, length = certchain == null ? 0 : certchain.length; i < length; i++)
			certlist.add(certchain[i]);

		certstore = new JcaCertStore(certlist);
		cert = keystore.getCertificate(KEY_ALIAS_IN_KEYSTORE);
		signer = new JcaContentSignerBuilder(SIGNATURE_ALGORITHM).setProvider("BC")
				.build((PrivateKey) (keystore.getKey(KEY_ALIAS_IN_KEYSTORE, KEYSTORE_PASSWORD.toCharArray())));
		generator.addSignerInfoGenerator(
				new JcaSignerInfoGeneratorBuilder(new JcaDigestCalculatorProviderBuilder().setProvider("BC").build())
						.build(signer, (X509Certificate) cert));
		generator.addCertificates(certstore);
	}

	/**
	 * @param content
	 * @return
	 * @throws CMSException
	 * @throws IOException
	 */
	public byte[] signData(final byte[] content) throws CMSException, IOException {
		CMSTypedData cmsdata = new CMSProcessableByteArray(content);
		// The boolean argument of the generate method indicates if the data will be embedded with the certificate or not.
		CMSSignedData signeddata = generator.generate(cmsdata, true);
		return signeddata.getEncoded();
	}
	
	/**
	 * @param msg
	 * @param alias
	 * @param password
	 * @return
	 * @throws NoSuchAlgorithmException 
	 * @throws KeyStoreException 
	 * @throws UnrecoverableKeyException 
	 * @throws InvalidKeyException 
	 * @throws SignatureException 
	 * @throws IOException 
	 * @throws InvalidKeySpecException 
	 */
	public String signWithPrivateKey(String msg, String path) throws UnrecoverableKeyException, KeyStoreException, NoSuchAlgorithmException, InvalidKeyException, SignatureException, IOException, InvalidKeySpecException {
		String BEGIN_RSA="-----BEGIN RSA PRIVATE KEY-----";
		String END_RSA="-----END RSA PRIVATE KEY-----";
		
		byte result[] = null;
		
		DataInputStream in=new DataInputStream(new FileInputStream(path));
		byte[] data=new byte[in.available()];
		in.readFully(data);
		in.close();
		
		String stringKey=new String(data);
		
		stringKey=stringKey.replaceAll(BEGIN_RSA, "");
		stringKey=stringKey.replaceAll(END_RSA, "");
		
		data=Base64.decode(stringKey.getBytes());
		PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(data);
		KeyFactory kf = KeyFactory.getInstance("RSA");
		PrivateKey privateKey = kf.generatePrivate(keySpec);
		
		Signature dsa = Signature.getInstance("SHA1withRSA");
		dsa.initSign(privateKey);
		dsa.update(msg.getBytes(), 0, msg.getBytes().length);
		result = dsa.sign();

		return new String(Base64.encode(result));
	}
	
	public String signWithDERFile(String msg, String path) throws UnrecoverableKeyException, KeyStoreException, NoSuchAlgorithmException, InvalidKeyException, SignatureException, IOException, InvalidKeySpecException {
		byte result[] = null;
		
		DataInputStream in=new DataInputStream(new FileInputStream(path));
		byte[] data=new byte[in.available()];
		in.readFully(data);
		in.close();
		
		PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(data);
		KeyFactory kf = KeyFactory.getInstance("RSA");
		PrivateKey privateKey = kf.generatePrivate(keySpec);
		
		Signature dsa = Signature.getInstance("SHA1withRSA");
		dsa.initSign(privateKey);
		dsa.update(msg.getBytes(), 0, msg.getBytes().length);
		result = dsa.sign();

		return new String(Base64.encode(result));
	}
	
	public String signWithoutBC(String msg) throws NoSuchAlgorithmException, InvalidKeyException, SignatureException, IOException, UnrecoverableKeyException, KeyStoreException{
		X509Certificate c = (X509Certificate) cert;
		byte aByteInfoToSign[] = msg.getBytes("UTF8");
		Signature signature = Signature.getInstance("Sha1WithRSA");
		signature.initSign((PrivateKey) (keystore.getKey(KEY_ALIAS_IN_KEYSTORE, KEYSTORE_PASSWORD.toCharArray())));
		signature.update(aByteInfoToSign);
		byte[] signedData = signature.sign();
		X500Name xName = new X500Name(c.getIssuerDN().getName());
		BigInteger serial = c.getSerialNumber();
		
		AlgorithmId digestAlgorithmId = new AlgorithmId(AlgorithmId.SHA_oid);
		AlgorithmId signAlgorithmId = new AlgorithmId(AlgorithmId.RSAEncryption_oid);
		SignerInfo sInfo = new SignerInfo(xName, serial, digestAlgorithmId, signAlgorithmId, signedData);
		ContentInfo cInfo = new ContentInfo(ContentInfo.DATA_OID, new DerValue(DerValue.tag_OctetString, aByteInfoToSign));
		PKCS7 p7 = new PKCS7(new AlgorithmId[] { digestAlgorithmId }, cInfo,new X509Certificate[] { c }, new SignerInfo[] { sInfo });
		
		ByteArrayOutputStream bOut = new DerOutputStream();
		p7.encodeSignedData(bOut);
		byte[] encodedPKCS7 = bOut.toByteArray();
		String result = new String(Base64.encode(encodedPKCS7));
		
		return result;
	}

	/**
	 * @param data
	 * @return
	 * @throws CMSException
	 * @throws CertificateException
	 * @throws OperatorCreationException
	 */
	public static boolean verifySignature(byte[] data) throws CMSException, CertificateException, OperatorCreationException {
		CMSSignedData cms = new CMSSignedData(data);
		Store store = cms.getCertificates();
		SignerInformationStore signers = cms.getSignerInfos();
		Collection c = signers.getSigners();
		Iterator it = c.iterator();

		while (it.hasNext()) {
			SignerInformation signer = (SignerInformation) it.next();
			Collection certCollection = store.getMatches(signer.getSID());
			Iterator certIt = certCollection.iterator();
			X509CertificateHolder certHolder = (X509CertificateHolder) certIt.next();
			X509Certificate cert = new JcaX509CertificateConverter().setProvider("BC").getCertificate(certHolder);
			if (signer.verify(new JcaSimpleSignerInfoVerifierBuilder().setProvider("BC").build(cert))) {
				return true;
			}
		}
		return false;
	}

	/**
	 * @param msg
	 * @return
	 * @throws CMSException
	 * @throws IOException
	 */
	public String getSignedContent(byte[] msg) throws CMSException, IOException {
		byte[] data = null;
		SignerInformation signer;
		Collection certCollection;
		Iterator certIt;
		X509CertificateHolder cert;
		CMSProcessable sc;

		CMSSignedData signature = new CMSSignedData(msg);
		Store cs = signature.getCertificates();
		SignerInformationStore signers = signature.getSignerInfos();
		Collection c = signers.getSigners();
		Iterator it = c.iterator();

		while (it.hasNext()) {
			signer = (SignerInformation) it.next();
			certCollection = cs.getMatches(signer.getSID());
			certIt = certCollection.iterator();
			cert = (X509CertificateHolder) certIt.next();
			sc = signature.getSignedContent();
			data = (byte[]) sc.getContent();
		}

		return new String(data);
	}

}
