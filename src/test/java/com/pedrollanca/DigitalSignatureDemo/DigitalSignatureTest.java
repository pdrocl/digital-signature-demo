package com.pedrollanca.DigitalSignatureDemo;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

import org.bouncycastle.cms.CMSException;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.util.encoders.Base64;

import junit.framework.TestCase;

public class DigitalSignatureTest extends TestCase {
	DigitalSignature ds=null;
	String message=null;
	
	protected void setUp() throws Exception {
		message="Test Message";
		ds=new DigitalSignature("test.p12", "privatekey123", "123456");
		super.setUp();
	}
	
	public void testDigitalSignature() throws UnrecoverableKeyException, KeyStoreException, NoSuchAlgorithmException, CertificateException, OperatorCreationException, IOException, CMSException {
	    assertNotNull(ds);
	}

	public void testSignData() throws CMSException, IOException {
		//It's important to explicitly indicate the content in UTF-8
	    byte signedData[]=ds.signData(message.getBytes("UTF-8"));
	    //Since the contents are bytes, the signed data must be converted to Base64 to be displayed
	    System.out.println(new String(Base64.encode(signedData)));
		assertNotNull(signedData);
	}

	public void testVerifySignature() throws CertificateException, OperatorCreationException, CMSException, UnsupportedEncodingException, IOException {
		byte signedData[]=ds.signData(message.getBytes("UTF-8"));
		assertTrue(ds.verifySignature(signedData));
	}

	public void testGetSignedContent() throws CMSException, IOException, InvalidKeyException, UnrecoverableKeyException, NoSuchAlgorithmException, SignatureException, KeyStoreException {
		//String s=ds.signWithoutBC(message);
		String s="MIIIEAYJKoZIhvcNAQcCoIIIATCCB/0CAQExCzAJBgUrDgMCGgUAMIG7BgkqhkiG9w0BBwGgga0EgaowMDAwMDA3NzEzMDA1NzU0MDIyMjtBRE1JTklTVFJBRE9SIEFWQU5aQURPICAgICAgICAgICAgICAgICAgICAgICAgICAgIDsgICAgICAgICAgICAgICAgICAgIDsgICAgICAgICAgICAgICAgICAgIDsgICAgICAgICAgICAgICAgICAgIDsxNTAuMjUwLjI1MS42MiA7MDcvMDIvMjAxNyAwNDo0OToxMKCCBVYwggVSMIIEOqADAgECAgIHXjANBgkqhkiG9w0BAQsFADCBojELMAkGA1UEBhMCTVgxGTAXBgNVBAgMEERpc3RyaXRvIEZlZGVyYWwxDzANBgNVBAcMBk1leGljbzEWMBQGA1UECgwNQkJWQSBCYW5jb21lcjESMBAGA1UECwwJU2VndXJpZGFkMREwDwYDVQQDDAhCQlZBIENDUjEoMCYGCSqGSIb3DQEJARYZU0lORkRZUkBiYnZhLmJhbmNvbWVyLmNvbTAeFw0xNjA5MTEwMzIyMzhaFw0yMTA5MTAwMzIyMzhaMIGJMQswCQYDVQQGEwJNWDEZMBcGA1UECBMQRGlzdHJpdG8gRmVkZXJhbDEPMA0GA1UEBxMGTWV4aWNvMRYwFAYDVQQKEw1CQlZBIEJhbmNvbWVyMRIwEAYDVQQLEwlTZWd1cmlkYWQxIjAgBgNVBAMTGVNlcnZpY2lvQ2VzaW9uRWxlY3Ryb25pY2EwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDH54bG+/nlObeLQRuWV1aMdPqqEcCwwdo1Zz9hhF4rKQ8Vo8CCraQ9fHMzNOwg2sL1WPxmZWVfRAuOrb7OO7KmMW7/24w69KEgaP9LeGoUBql10w+0k0vZy3O8xrnxcbx9DLDdama3s/KesRwxrLhLVgvp5VzgBOTlUEvqC6cAfQ95V0SvHZN1wcRloMJ2Em2QIP5vyV16iCyKgfjfuJtkEuodke0fpYv0R5Ie2ODyHzC2B5+kAwD1FaEZT2unn02NQHebrbxdYCq8+pC43lIBtvhMK5mblvbe8qURxB4H6QdIbYJTU3skAB605p8Z1gMa5e1ZzlH6tPf5Fs5NMLEJAgMBAAGjggGnMIIBozAJBgNVHRMEAjAAMB8GCWCGSAGG+EIBDQQSFhBCQlZBIEJhbmNvbWVyIENBMB0GA1UdDgQWBBTpAF98j/44iYPvoklylOE2qdjguDAfBgNVHSMEGDAWgBRiwZ5OU62jLWjcc2GBTSZMBIfsgTAqBgNVHSUBAf8EIDAeBggrBgEFBQcDAQYIKwYBBQUHAwIGCCsGAQUFBwMDMIGTBggrBgEFBQcBAQSBhjCBgzAxBggrBgEFBQcwAYYlaHR0cDovL2JidmFjZXJ0LmludHJhbmV0LmNvbS5teDoxOTg0LzBOBggrBgEFBQcwAoZCaHR0cDovL2JidmFjZXJ0LmludHJhbmV0LmNvbS5teDo4MDgwL0dlc3Rpb25DQS13YXIvQ0EvMC9jYWNlcnQuY2VyMDoGCWCGSAGG+EIBBAQtFitodHRwOi8vYmJ2YWNlcnQuaW50cmFuZXQuY29tLm14L2JidmEtY2EuY3JsMDcGCWCGSAGG+EIBAwQqFihodHRwOi8vYmJ2YWNlcnQuaW50cmFuZXQuY29tLm14L2JidmEuY3JsMA0GCSqGSIb3DQEBCwUAA4IBAQACV8enDTn9I5H8XfFCy9PTU2XEXZU8JMEV1u4o04AgOHAa35PLYCy6Zpa7e+SzmbV8mkeRb5gNCNiUcdX6JLmGSdkS/GESK57GwcOjFxFxYBl2hJ5MT1BLM+O480BqQtg9jUORsvJUieORPpleQ66hwTfOCvz/fkeOF/OevPsGSwhntxTFW4ad2GR6bUAktTLm1ctKGScE0ODoIkwbNzDNg27Y/MJU6O6qXysjYU/BclNpvtI2WMrOqCOnD1oMjW8ZT7OG2CCULBv7ATEkCJiOJ2txZockgWlUxGD8X9s/9BZ1Op2UM50hbUZ/hAjpmcJxCOSoE+a4kp6dlJM8cq9ZMYIB0TCCAc0CAQEwgakwgaIxCzAJBgNVBAYTAk1YMRkwFwYDVQQIExBEaXN0cml0byBGZWRlcmFsMQ8wDQYDVQQHEwZNZXhpY28xFjAUBgNVBAoTDUJCVkEgQmFuY29tZXIxEjAQBgNVBAsTCVNlZ3VyaWRhZDERMA8GA1UEAxMIQkJWQSBDQ1IxKDAmBgkqhkiG9w0BCQEWGVNJTkZEWVJAYmJ2YS5iYW5jb21lci5jb20CAgdeMAkGBSsOAwIaBQAwDQYJKoZIhvcNAQEBBQAEggEAWN5Ke9lBuAewxYW9Dh+8//gj+czGkRLrtRIISjpWP4PAcJg9s04chvrM4AU4mnlI9u39vPw3mlJcr+PXaMHof/EHkki24iAcmq77IZrHipfkoBs4WS06vdQQNELBEs2HdIC5XGu1BWPXn+T8SMcY1cd9mEEuePypUANCQTpPpKJR01DgQEJXnfuE7DY5VQB/bE+7UYjat/82VzDJREbwWYID/IlDS4VBMlgGIhXeljYx8OwXB3CqoOCOAI9JXrnO1sXUqgzJE8h2TwQdRg8WNvaz+MWFaRBfFQzS2TyOnrJwm5OTTVfO//TZf3XRpp/pPTU0VZqH18p6GdJHnZqfWA==";
		System.out.println("s="+s);
		byte signedData[]=s.getBytes("UTF-8");
		assertNotNull(ds.getSignedContent(Base64.decode(s)));
		System.out.println("mensaje:"+ds.getSignedContent(Base64.decode(s)));
	}
	
	public void testSignWithPrivateKey() throws Exception{
		System.out.println(ds.signWithPrivateKey(message, "APIprivate.key"));
		assertNotNull(ds.signWithPrivateKey(message, "APIprivate.key"));
	}
	
	public void testSignWithDERFile() throws Exception{
		System.out.println(ds.signWithDERFile(message, "APIprivate.der"));
		assertNotNull(ds.signWithDERFile(message, "APIprivate.der"));
	}
}
